package update

import (
	"fmt"
	"io/ioutil"
	"os"

	"github.com/mmcdole/gofeed"
	"gitlab.com/codeprac/cms/internal/cms"
	"gitlab.com/codeprac/cms/internal/data"
	"gitlab.com/codeprac/modules/go/log"
	"gopkg.in/yaml.v2"
)

func loadDataFile(pathToDataFile string) (*data.File, error) {
	log.Debugf("loading data file from '%s'...", pathToDataFile)

	dataFileInfo, err := os.Lstat(pathToDataFile)
	if err != nil {
		return nil, fmt.Errorf("failed to validate path '%s': %s", pathToDataFile, err)
	}
	if dataFileInfo.IsDir() {
		return nil, fmt.Errorf("failed to find a file at '%s'", pathToDataFile)
	}
	dataFileContents, err := ioutil.ReadFile(pathToDataFile)
	if err != nil {
		return nil, fmt.Errorf("failed to read file at '%s': %s", pathToDataFile, err)
	}
	var dataFile data.File
	if err := yaml.Unmarshal(dataFileContents, &dataFile); err != nil {
		return nil, fmt.Errorf("failed to unmarshal data file: %s", err)
	}

	log.Infof("successfully loaded data file from '%s'", pathToDataFile)
	return &dataFile, nil
}

func parseDataFile(file data.File) cms.Feeds {
	feeds := cms.Feeds{}
	feedParser := gofeed.NewParser()
	numberOfFeeds := len(file.Feeds)
	log.Debugf("retrieving data for %v feeds...", numberOfFeeds)

	for index, feed := range file.Feeds {
		log.Debugf("[%v/%v] processing feed with url '%s'...", index+1, numberOfFeeds, feed.URL)
		gofeedFeed, err := feedParser.ParseURL(feed.URL)
		if err != nil {
			log.Warnf("failed to get feed data for '%s': %s", feed.URL, err)
			continue
		}
		log.Infof("[%v/%v] adding '%s' to list of feeds...", index+1, numberOfFeeds, feed.URL)
		feeds = append(feeds, *cms.NewFeedFromGofeed(gofeedFeed, feed.URL))
	}

	log.Infof("successfully parsed %v/%v feeds", len(feeds), numberOfFeeds)
	return feeds
}
