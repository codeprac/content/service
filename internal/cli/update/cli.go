package update

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/codeprac/cms/internal/cli/utils"
	databaseconfiguration "gitlab.com/codeprac/cms/internal/database/configuration"
	"gitlab.com/codeprac/cms/internal/database/connection"
	"gitlab.com/codeprac/modules/go/log"
)

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "update",
		Short: "Reads from file data, retrieves RSS data, and upserts info into the database",
		RunE: func(cmd *cobra.Command, args []string) error {
			connection, err := utils.ConnectToMongo(connection.MongoConnectionOpts{
				Hostnames:    conf.GetStringSlice(databaseconfiguration.KeyHostnames),
				Port:         conf.GetUint(databaseconfiguration.KeyPort),
				User:         conf.GetString(databaseconfiguration.KeyUsername),
				Password:     conf.GetString(databaseconfiguration.KeyPassword),
				Database:     conf.GetString(databaseconfiguration.KeyDatabase),
				AuthSource:   conf.GetString(databaseconfiguration.KeyAuthSource),
				IsTlsEnabled: conf.GetBool(databaseconfiguration.KeyUseTLS),
			})
			if err != nil {
				return fmt.Errorf("failed to connect to mongo: %s", err)
			}
			disconnectCtx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
			defer cancel()
			defer connection.Disconnect(disconnectCtx)
			db := connection.Database(conf.GetString(databaseconfiguration.KeyDatabase))

			dataFile, err := loadDataFile(conf.GetString(KeyDataFile))
			if err != nil {
				return fmt.Errorf("failed to ingest data files: %s", err)
			}

			feeds := parseDataFile(*dataFile)

			log.Debugf("adding %v feeds to database...", len(feeds))
			feedIDs, err := feeds.Upsert(connection.Database(conf.GetString(databaseconfiguration.KeyDatabase)))
			if err != nil {
				return fmt.Errorf("failed to update feeds in the database: %s", err)
			}
			log.Infof("successfully inserted/updated %v feeds: ['%s']", len(feedIDs), strings.Join(feedIDs, "', '"))

			log.Debugf("adding items from %v feeds to database...", len(feeds))
			for index, feed := range feeds {
				log.Debugf("[%v/%v] inserting %v items from '%s' into the database...", index+1, len(feeds), len(feed.Items), feed.GetKey())
				if ids, err := feed.Items.Upsert(db); err != nil {
					log.Warnf("[%v/%v] failed to update item data for '%s': %s", index+1, len(feeds), feed.GetKey(), err)
				} else {
					log.Infof("[%v/%v] successfully inserted %v items belonging to '%s': ['%s']", index+1, len(feeds), len(ids), feed.GetKey(), strings.Join(ids, "', '"))
				}
			}
			return nil
		},
	}

	conf.ApplyToCobra(&command)
	return &command
}
