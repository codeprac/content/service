package utils

import (
	"context"
	"fmt"
	"strings"
	"time"

	"gitlab.com/codeprac/cms/internal/database/connection"
	"gitlab.com/codeprac/modules/go/log"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

func ConnectToMongo(opts connection.MongoConnectionOpts) (*mongo.Client, error) {
	log.Debugf("setting up database connection...")
	log.Debugf("connecting to database '%s' at [%s] with user '%s'", opts.Database, strings.Join(opts.Hostnames, ","), opts.User)
	connection, err := connection.NewMongoConnection(opts)
	if err != nil {
		return nil, fmt.Errorf("failed to establish connection with the database: %s", err)
	}
	connectionPingCtx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if err := connection.Ping(connectionPingCtx, readpref.PrimaryPreferred()); err != nil {
		return nil, fmt.Errorf("failed to complete a ping with the database: %s", err)
	}

	log.Infof("successfully connected to the databse")
	return connection, nil
}
