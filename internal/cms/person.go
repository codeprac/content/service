package cms

import (
	"strings"

	pb "gitlab.com/codeprac/cms/pkg/cms"
	"go.mongodb.org/mongo-driver/bson"
)

const (
	PersonSearchKey = "key"
)

func NewPersonFromProtobuf(p *pb.Person) *Person {
	return &Person{
		ID:       int(p.GetId()),
		ObjectID: p.GetObjectID(),
		UUID:     p.GetUuid(),
		Name:     p.GetName(),
		Email:    p.GetEmail(),
		URI:      p.GetUri(),
	}
}

type Person struct {
	ID       int    `json:"-"`
	ObjectID string `json:"id" bson:"_id"`
	UUID     string `json:"uuid,omitempty"`
	Name     string `json:"name"`
	Email    string `json:"email"`
	URI      string `json:"uri"`
}

func (person Person) GetFilterDocument() bson.M {
	return bson.M{PersonSearchKey: person.GetKey()}
}

func (person Person) GetKey() string {
	return strings.ToLower(person.Name + person.Email)
}

func (person Person) GetUpdateDocument() bson.M {
	return bson.M{
		"$set": bson.M{
			PersonSearchKey: person.GetKey(),
			"email":         strings.ToLower(person.Email),
			"name":          person.Name,
			"uri":           person.URI,
		},
	}
}

func (person Person) ToProtobuf() *pb.Person {
	return &pb.Person{
		ObjectID: person.ObjectID,
		Name:     person.Name,
		Email:    person.Email,
		Uri:      person.URI,
	}
}
