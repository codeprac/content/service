/*
	Package cms contains Engine and ResourceAccess layer components for
	internal use. If you intend to call this service via gRPC, the
	package you are looking for is at gitlab.com/codeprac/cms/pkg/cms,
	not this one (same depednency, different path).

	This package contains:

	- type definitions for all content types
	- content (de)serializers
	- database access mechanisms
*/
package cms
