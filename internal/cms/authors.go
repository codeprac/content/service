package cms

import (
	"context"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type GetAuthorsOpts struct {
	Database  *mongo.Database
	AuthorIDs []string
	QueryOpts
}

func GetAuthors(opts GetAuthorsOpts) (People, error) {
	collection := opts.Database.Collection(CollectionAuthors)
	findContext, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	filter := bson.M{}
	if opts.AuthorIDs != nil && len(opts.AuthorIDs) > 0 {
		filter[PersonSearchKey] = bson.M{
			"$in": opts.AuthorIDs,
		}
	}

	find := options.Find()
	if opts.QueryOpts.SortKey != "" && opts.QueryOpts.SortDirection != 0 {
		find.SetSort(bson.M{opts.QueryOpts.SortKey: opts.QueryOpts.SortDirection})
	}
	limit := DefaultAuthorLimit
	if opts.QueryOpts.Limit > 0 {
		limit = opts.QueryOpts.Limit
	}
	find.SetLimit(limit)
	find.SetSkip(opts.QueryOpts.Offset)

	result, err := collection.Find(findContext, filter, find)
	if err != nil {
		return nil, fmt.Errorf("failed to retrieve authors from database: %s", err)
	}
	authors := People{}
	if err := result.All(context.Background(), &authors); err != nil {
		return nil, fmt.Errorf("failed to parse authors list: %s", err)
	}
	return authors, nil
}
