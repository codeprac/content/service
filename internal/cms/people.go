package cms

import (
	"fmt"

	"github.com/mmcdole/gofeed"
	pb "gitlab.com/codeprac/cms/pkg/cms"
	"go.mongodb.org/mongo-driver/mongo"
)

func NewPeopleFromGofeed(p []*gofeed.Person) People {
	people := People{}
	for _, person := range p {
		people = append(people, Person{Name: person.Name, Email: person.Email})
	}
	return people
}

func NewPeopleFromProtobuf(p []*pb.Person) People {
	people := People{}
	for _, pbPerson := range p {
		people = append(people, *NewPersonFromProtobuf(pbPerson))
	}
	return people
}

type People []Person

func (people People) ToProtobuf() []*pb.Person {
	pbPeople := []*pb.Person{}
	for _, person := range people {
		pbPeople = append(pbPeople, person.ToProtobuf())
	}
	return pbPeople
}

func (people People) Upsert(db *mongo.Database, collectionName string) ([]string, error) {
	if len(people) == 0 {
		return []string{}, nil
	}
	operations := []mongo.WriteModel{}
	targetPeople := []string{}
	for _, person := range people {
		operations = append(operations, newUpsertOperation(person))
		targetPeople = append(targetPeople, person.GetKey())
	}
	if err := bulkWrite(operations, db.Collection(collectionName)); err != nil {
		return nil, fmt.Errorf("failed to upsert people: %s", err)
	}
	return getIds(getIdsOpts{
		CollectionName: collectionName,
		Database:       db,
		Keys:           targetPeople,
		SearchKey:      PersonSearchKey,
	})
}
