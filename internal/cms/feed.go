package cms

import (
	"strings"
	"time"

	"github.com/mmcdole/gofeed"
	pb "gitlab.com/codeprac/cms/pkg/cms"
	"go.mongodb.org/mongo-driver/bson"
)

const (
	FeedSearchKey = "rssURL"
)

// NewFeedFromGofeed serializes a gofeed.Feed instance into our own Feed instance
func NewFeedFromGofeed(f *gofeed.Feed, rssURL string) *Feed {
	feed := &Feed{
		Title:       f.Title,
		Description: f.Description,
		Link:        f.Link,
		RssURL:      rssURL,
		Categories:  NewCategoriesFromString(f.Categories),
		Authors:     NewPeopleFromGofeed(f.Authors),
		Items:       NewItemsFromGofeed(f.Items),
	}
	if f.Image != nil {
		feed.ImageURL = f.Image.URL
	}
	if f.UpdatedParsed != nil {
		feed.Updated = *f.UpdatedParsed
	}
	if f.PublishedParsed != nil {
		feed.Published = *f.PublishedParsed
	}
	return feed
}

type Feed struct {
	ID          int        `json:"-"`
	ObjectID    string     `json:"id" bson:"_id"`
	UUID        string     `json:"uuid,omitempty"`
	Title       string     `json:"title"`
	Description string     `json:"description"`
	ImageURL    string     `json:"imageURL"`
	Link        string     `json:"link"`
	RssURL      string     `json:"rssURL"`
	Updated     time.Time  `json:"updated"`
	Published   time.Time  `json:"published"`
	Categories  Categories `json:"categories" bson:"-"`
	CategoryIDs []string   `json:"categoryIDs" bson:"categories" `
	Authors     People     `json:"authors" bson:"-"`
	AuthorIDs   []string   `json:"authorIDs" bson:"authors" `
	Items       Items      `json:"items" bson:"-"`
	ItemIDs     []string   `json:"itemIDs" bson:"items" `
}

func (feed Feed) GetFilterDocument() bson.M {
	return bson.M{FeedSearchKey: feed.GetKey()}
}

func (feed Feed) GetKey() string {
	return strings.Trim(strings.ToLower(feed.RssURL), "/")
}

func (feed Feed) GetUpdateDocument() bson.M {
	return bson.M{"$set": bson.M{
		FeedSearchKey: feed.GetKey(),
		"link":        feed.Link,
		"title":       feed.Title,
		"description": feed.Description,
		"imageURL":    feed.ImageURL,
		"updated":     feed.Updated,
		"published":   feed.Published,
		"authors":     feed.AuthorIDs,
		"categories":  feed.CategoryIDs,
	}}
}

func (feed Feed) ToProtobuf() *pb.Feed {
	return &pb.Feed{
		ObjectID:    feed.ObjectID,
		Title:       feed.Title,
		Description: feed.Description,
		ImageURL:    feed.ImageURL,
		Link:        feed.Link,
		RssURL:      feed.RssURL,
		Updated:     feed.Updated.Format(TimestampFormatFeeds),
		Published:   feed.Published.Format(TimestampFormatFeeds),
		CategoryIDs: feed.CategoryIDs,
		AuthorIDs:   feed.AuthorIDs,
	}
}
