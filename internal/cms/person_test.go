package cms

import (
	"testing"

	"github.com/stretchr/testify/suite"
	pb "gitlab.com/codeprac/cms/pkg/cms"
	"go.mongodb.org/mongo-driver/bson"
)

type PersonTests struct {
	suite.Suite
	isUpsertable upsertable
}

func TestPerson(t *testing.T) {
	suite.Run(t, new(PersonTests))
}

// Test__InterfaceConstraints will prevent tests from running if `Person`
// no longer implements `upsertable`
func (s PersonTests) Test__InterfaceConstraints() {
	s.isUpsertable = Person{}
}

func (s PersonTests) Test_NewPersonFromProtobuf() {
	pbPerson := &pb.Person{
		Id:       1,
		ObjectID: "object_id",
		Uuid:     "uuid",
		Name:     "name",
		Email:    "email",
		Uri:      "uri",
	}
	person := NewPersonFromProtobuf(pbPerson)
	s.Equal(person.ID, 1)
	s.Equal(person.ObjectID, "object_id")
	s.Equal(person.UUID, "uuid")
	s.Equal(person.Name, "name")
	s.Equal(person.Email, "email")
	s.Equal(person.URI, "uri")
}

func (s PersonTests) Test_Person_GetKey() {
	person := Person{Name: "NamE", Email: "eMail@domAin.com"}
	s.Equal(person.GetKey(), "nameemail@domain.com")
}

func (s PersonTests) Test_Person_FilterAndUpdateMatches() {
	person := Person{Name: "NamE", Email: "eMail@domAin.com"}
	filterDocument := person.GetFilterDocument()
	updateDocument := person.GetUpdateDocument()
	updateDocumentSetter := updateDocument["$set"].(bson.M)
	s.Equal(updateDocumentSetter[PersonSearchKey], filterDocument[PersonSearchKey])
}

func (s PersonTests) Test_Person_GetUpdateDocument() {
	person := Person{Name: "NamE", Email: "eMail@domAin.com", URI: "uri"}
	updateDocumentSetter := person.GetUpdateDocument()["$set"].(bson.M)
	s.Contains(updateDocumentSetter, "email")
	s.Contains(updateDocumentSetter, "name")
	s.Contains(updateDocumentSetter, "uri")
}

func (s PersonTests) Test_Person_ToProtobuf() {
	person := Person{ObjectID: "objectID", Name: "NamE", Email: "eMail@domAin.com"}
	pbPerson := person.ToProtobuf()
	s.Equal(person.ObjectID, pbPerson.ObjectID)
	s.Equal(person.Name, pbPerson.Name)
	s.Equal(person.Email, pbPerson.Email)
	s.Equal(person.URI, pbPerson.Uri)
}
