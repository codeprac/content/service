package cms

import (
	"strings"

	pb "gitlab.com/codeprac/cms/pkg/cms"
	"go.mongodb.org/mongo-driver/bson"
)

const (
	CategorySearchKey = "name"
)

// NewCategoryFromProtobuf is a serializer for converting a protobuf-defined Category
// into a natively understood Category instance
func NewCategoryFromProtobuf(c *pb.Category) *Category {
	return &Category{
		ID:       int(c.GetId()),
		ObjectID: c.GetObjectID(),
		UUID:     c.GetUuid(),
		Name:     c.GetName(),
	}
}

type Category struct {
	ID       int    `json:"-"`
	ObjectID string `json:"id" bson:"_id"`
	UUID     string `json:"uuid,omitempty"`
	Name     string `json:"name"`
}

func (category Category) GetFilterDocument() bson.M {
	return bson.M{CategorySearchKey: category.GetKey()}
}

// GetKey returns a normalised variant of what the category key should be
func (category Category) GetKey() string {
	return strings.ToLower(category.Name)
}

// GetUpdateDocument returns what the Category should look like in the database
func (category Category) GetUpdateDocument() bson.M {
	return bson.M{
		"$set": bson.M{
			CategorySearchKey: category.GetKey(),
		},
	}
}

// ToProtobuf converts this Category into its protobuf structure
func (category Category) ToProtobuf() *pb.Category {
	return &pb.Category{
		ObjectID: category.ObjectID,
		Name:     category.Name,
	}
}
