package cms

import (
	"context"
	"fmt"
	"time"

	pb "gitlab.com/codeprac/cms/pkg/cms"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type GetCategoriesOpts struct {
	Database    *mongo.Database
	CategoryIDs []string
	QueryOpts
}

func GetCategories(opts GetCategoriesOpts) (Categories, error) {
	collection := opts.Database.Collection(CollectionCategories)
	findContext, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	filter := bson.M{}
	if opts.CategoryIDs != nil && len(opts.CategoryIDs) > 0 {
		filter[CategorySearchKey] = bson.M{
			"$in": opts.CategoryIDs,
		}
	}

	find := options.Find()
	if opts.QueryOpts.SortKey != "" && opts.QueryOpts.SortDirection != 0 {
		find.SetSort(bson.M{opts.QueryOpts.SortKey: opts.QueryOpts.SortDirection})
	}
	limit := DefaultCategoryLimit
	if opts.QueryOpts.Limit > 0 {
		limit = opts.QueryOpts.Limit
	}
	find.SetLimit(limit)
	find.SetSkip(opts.QueryOpts.Offset)

	result, err := collection.Find(findContext, filter, find)
	if err != nil {
		return nil, fmt.Errorf("failed to retrieve categories from database: %s", err)
	}
	categories := Categories{}
	if err := result.All(context.Background(), &categories); err != nil {
		return nil, fmt.Errorf("failed to parse categories list: %s", err)
	}
	return categories, nil
}

// NewCategoriesFromString is a serializer for converting strings into Category instances
func NewCategoriesFromString(categories []string) Categories {
	cs := Categories{}
	for _, category := range categories {
		cs = append(cs, Category{Name: category})
	}
	return cs
}

// NewCategoriesFromProtobuf is a serializer for converting protobuf-defined categories into
// natively understood categories
func NewCategoriesFromProtobuf(c []*pb.Category) Categories {
	categories := Categories{}
	for _, pbCategory := range c {
		categories = append(categories, *NewCategoryFromProtobuf(pbCategory))
	}
	return categories
}

// Categories represents a list of Category instances with batteries attached
type Categories []Category

// ToProtobuf converts Category instances in this Categories instance into
// a slice of protobuf Category instances
func (categories Categories) ToProtobuf() []*pb.Category {
	pbCategories := []*pb.Category{}
	for _, category := range categories {
		pbCategories = append(pbCategories, category.ToProtobuf())
	}
	return pbCategories
}

// Upsert orchestrates the upserting of this list of Category instances into the database
//  and returns a list of their database IDs
func (categories Categories) Upsert(db *mongo.Database) ([]string, error) {
	if len(categories) == 0 {
		return []string{}, nil
	}
	operations := []mongo.WriteModel{}
	targetCategories := []string{}
	for _, category := range categories {
		operations = append(operations, newUpsertOperation(category))
		targetCategories = append(targetCategories, category.GetKey())
	}
	if err := bulkWrite(operations, db.Collection(CollectionCategories)); err != nil {
		return nil, fmt.Errorf("failed to upsert categories: %s", err)
	}
	return getIds(getIdsOpts{
		CollectionName: CollectionCategories,
		Database:       db,
		Keys:           targetCategories,
		SearchKey:      CategorySearchKey,
	})
}
