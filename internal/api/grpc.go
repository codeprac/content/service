package api

import (
	"context"
	"fmt"

	"gitlab.com/codeprac/cms/internal/cms"
	pb "gitlab.com/codeprac/cms/pkg/cms"
	"go.mongodb.org/mongo-driver/mongo"
	"google.golang.org/grpc"
)

// CmsServer is the implementation of the grpc interface defined by the cms service
type CmsServer struct {
	pb.UnimplementedContentManagementSystemServer
	Database *mongo.Database
}

// GetAuthors returns a full list of authors
func (s *CmsServer) GetAuthors(ctx context.Context, queryOpts *pb.QueryOpts) (*pb.Authors, error) {
	limit, offset := getQueryOptsFromGrpcRequest(queryOpts, cms.DefaultAuthorLimit, 0)
	authors, err := cms.GetAuthors(cms.GetAuthorsOpts{
		Database: s.Database,
		QueryOpts: cms.QueryOpts{
			Limit:  limit,
			Offset: offset,
		},
	})
	if err != nil {
		return nil, fmt.Errorf("failed to get authors: %s", err)
	}
	pbAuthors := authors.ToProtobuf()
	res := pb.Authors{
		Data: pbAuthors,
	}
	return &res, nil
}

// GetCateories returns a full list of categories
func (s *CmsServer) GetCategories(ctx context.Context, queryOpts *pb.QueryOpts) (*pb.Categories, error) {
	limit, offset := getQueryOptsFromGrpcRequest(queryOpts, cms.DefaultCategoryLimit, 0)
	categories, err := cms.GetCategories(cms.GetCategoriesOpts{
		Database: s.Database,
		QueryOpts: cms.QueryOpts{
			Limit:  limit,
			Offset: offset,
		},
	})
	if err != nil {
		return nil, fmt.Errorf("failed to get categories: %s", err)
	}
	pbCategories := categories.ToProtobuf()
	res := pb.Categories{
		Data: pbCategories,
	}
	return &res, nil
}

// GetFeeds returns a full list of feeds
func (s *CmsServer) GetFeeds(ctx context.Context, queryOpts *pb.QueryOpts) (*pb.Feeds, error) {
	limit, offset := getQueryOptsFromGrpcRequest(queryOpts, cms.DefaultFeedLimit, 0)
	feeds, err := cms.GetFeeds(cms.GetFeedsOpts{
		Database: s.Database,
		QueryOpts: cms.QueryOpts{
			Limit:  limit,
			Offset: offset,
		},
	})
	if err != nil {
		return nil, fmt.Errorf("failed to get feeds: %s", err)
	}
	pbFeeds := feeds.ToProtobuf()
	res := pb.Feeds{
		Data: pbFeeds,
	}
	return &res, nil
}

// GetItems returns a full list of items
func (s *CmsServer) GetItems(ctx context.Context, queryOpts *pb.QueryOpts) (*pb.Items, error) {
	limit, offset := getQueryOptsFromGrpcRequest(queryOpts, cms.DefaultItemLimit, 0)
	items, err := cms.GetItems(cms.GetItemsOpts{
		Database: s.Database,
		QueryOpts: cms.QueryOpts{
			Limit:         limit,
			Offset:        offset,
			SortKey:       "published",
			SortDirection: -1,
		},
	})
	if err != nil {
		return nil, fmt.Errorf("failed to get items: %s", err)
	}
	pbItems := items.ToProtobuf()
	res := pb.Items{
		Data: pbItems,
	}
	return &res, nil
}

type GrpcHandlerOpts struct {
	Database *mongo.Database
	Instance *grpc.Server
}

func RegisterGrpcHandlers(opts GrpcHandlerOpts) {
	pb.RegisterContentManagementSystemServer(opts.Instance, &CmsServer{
		Database: opts.Database,
	})
}

func getQueryOptsFromGrpcRequest(queryOpts *pb.QueryOpts, defaultLimit, defaultOffset int64) (limit, offset int64) {
	limit = defaultLimit
	if queryOpts.Limit > 0 {
		limit = queryOpts.Limit
	}
	offset = defaultOffset
	if queryOpts.Offset > 0 {
		offset = queryOpts.Offset
	}
	return limit, offset
}
