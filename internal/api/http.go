package api

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
	"gitlab.com/codeprac/cms/internal/cms"
	"gitlab.com/codeprac/modules/go/log"
	"gitlab.com/codeprac/modules/go/server/responder"
	"go.mongodb.org/mongo-driver/mongo"
)

type HttpHandlerOpts struct {
	Database *mongo.Database
}

func (o HttpHandlerOpts) Validate() error {
	errors := []string{}

	if o.Database == nil {
		errors = append(errors, "missing db")
	}

	if len(errors) > 0 {
		return fmt.Errorf("failed to validate options for creating a http handler: ['%s']", strings.Join(errors, "', '"))
	}
	return nil
}

func GetHttpHandler(opts HttpHandlerOpts) (http.Handler, error) {
	if err := opts.Validate(); err != nil {
		return nil, err
	}

	handler := mux.NewRouter()

	respondWith := responder.HTTP{
		Source:      "api",
		ErrorLogger: responder.LogAs(log.Warnf),
	}

	handler.Handle("/", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		respondWith.OK("practice makes perfect").Send(w)
	}))

	handler.Handle("/authors", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		limit, offset := getQueryOptsFromHttpRequest(r, cms.DefaultAuthorLimit, 0)
		authors, err := cms.GetAuthors(cms.GetAuthorsOpts{
			Database: opts.Database,
			QueryOpts: cms.QueryOpts{
				Limit:  limit,
				Offset: offset,
			},
		})
		if err != nil {
			log.Warnf("failed to get authors: %s", err)
			respondWith.InternalServerError("get_authors_db").Send(w)
			return
		}
		respondWith.OK(authors).Send(w)
	})).Methods(http.MethodGet)

	handler.Handle("/categories", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		limit, offset := getQueryOptsFromHttpRequest(r, cms.DefaultCategoryLimit, 0)
		categories, err := cms.GetCategories(cms.GetCategoriesOpts{
			Database: opts.Database,
			QueryOpts: cms.QueryOpts{
				Limit:  limit,
				Offset: offset,
			},
		})
		if err != nil {
			log.Warnf("failed to get categories: %s", err)
			respondWith.InternalServerError("get_categories_db").Send(w)
			return
		}
		respondWith.OK(categories).Send(w)
	})).Methods(http.MethodGet)

	handler.Handle("/feeds", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		limit, offset := getQueryOptsFromHttpRequest(r, cms.DefaultFeedLimit, 0)
		feeds, err := cms.GetFeeds(cms.GetFeedsOpts{
			Database: opts.Database,
			QueryOpts: cms.QueryOpts{
				Limit:  limit,
				Offset: offset,
			},
		})
		if err != nil {
			log.Warnf("failed to get feeds: %s", err)
			respondWith.InternalServerError("get_feeds_db").Send(w)
			return
		}
		respondWith.OK(feeds).Send(w)
	})).Methods(http.MethodGet)

	handler.Handle("/items", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		limit, offset := getQueryOptsFromHttpRequest(r, cms.DefaultItemLimit, 0)
		items, err := cms.GetItems(cms.GetItemsOpts{
			Database: opts.Database,
			QueryOpts: cms.QueryOpts{
				Limit:         limit,
				Offset:        offset,
				SortKey:       "published",
				SortDirection: -1,
			},
		})
		if err != nil {
			log.Warnf("failed to get items: %s", err)
			respondWith.InternalServerError("get_items_db").Send(w)
			return
		}
		respondWith.OK(items).Send(w)
	})).Methods(http.MethodGet)

	return handler, nil
}

func getQueryOptsFromHttpRequest(r *http.Request, defaultLimit, defaultOffset int64) (limit int64, offset int64) {
	limit = defaultLimit
	queryLimit := r.FormValue("limit")
	if l, err := strconv.ParseInt(queryLimit, 10, 0); err == nil {
		limit = l
	}
	offset = defaultOffset
	queryOffset := r.FormValue("offset")
	if o, err := strconv.ParseInt(queryOffset, 10, 0); err == nil {
		offset = o
	}
	return limit, offset
}
