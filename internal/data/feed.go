package data

type Feed struct {
	URL string `yaml:"url"`
}

type Feeds []Feed
