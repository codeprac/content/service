package connection

import (
	"context"
	"fmt"
	"net/url"
	"strings"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	DefaultConnectionTimeout = 15 * time.Second
)

type MongoConnectionOpts struct {
	Hostnames    []string
	Port         uint
	User         string
	Password     string
	Database     string
	AuthSource   string
	IsTlsEnabled bool
	Timeout      time.Duration
}

func NewMongoConnection(opts MongoConnectionOpts) (*mongo.Client, error) {
	connectionTimeout := DefaultConnectionTimeout
	if opts.Timeout != 0 {
		connectionTimeout = opts.Timeout
	}
	ctx, cancel := context.WithTimeout(context.Background(), connectionTimeout)
	defer cancel()

	queryValues := url.Values{}
	queryValues.Add("authSource", opts.AuthSource)
	queryValues.Add("tls", fmt.Sprintf("%v", opts.IsTlsEnabled))
	queryValues.Add("w", "majority")
	queryString := fmt.Sprintf(queryValues.Encode())

	uri := fmt.Sprintf("mongodb://%s:%s@%s:%v/%s?%s",
		opts.User,
		opts.Password,
		strings.Join(opts.Hostnames, ","),
		opts.Port,
		opts.Database,
		queryString,
	)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(uri))
	if err != nil {
		return nil, fmt.Errorf("failed to connect to mongo: %s", err)
	}

	return client, nil
}
