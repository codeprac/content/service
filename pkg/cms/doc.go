/*
	Package cms contains the protobuf definitions for interacting with the
	cms service from within the network.
*/
package cms

//go:generate protoc --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative ./cms.proto
