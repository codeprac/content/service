FROM golang:1.16-alpine AS build
RUN apk add --no-cache make g++ ca-certificates
WORKDIR /go/src/app
COPY ./go.mod ./go.sum ./
RUN go mod download -x
COPY ./cmd ./cmd
COPY ./internal ./internal
COPY ./pkg ./pkg
COPY ./Makefile ./
ARG VERSION=latest
ENV VERSION=${VERSION}
RUN make build
RUN mv ./bin/cms_$(go env GOOS)_$(go env GOARCH) ./bin/cms
RUN mv ./bin/cms_$(go env GOOS)_$(go env GOARCH).sha256 ./bin/cms.sha256

FROM scratch AS final
COPY --from=build /go/src/app/bin/cms /cms
COPY --from=build /go/src/app/bin/cms.sha256 /cms.sha256
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
CMD ["/cms"]
VOLUME ["/data"]
