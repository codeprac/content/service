# Experimentation

This document covers manual "testing" and other non-automated things you can do to check out what the service can do in the development environment.

- [Experimentation](#experimentation)
  - [Pre-requisites](#pre-requisites)
  - [CLI commands](#cli-commands)
    - [Populate the database](#populate-the-database)
    - [Start the server](#start-the-server)
  - [Getting stuff](#getting-stuff)
    - [Getting authors](#getting-authors)
    - [Getting categories](#getting-categories)
    - [Getting feeds](#getting-feeds)
    - [Getting items](#getting-items)

## Pre-requisites

1. Ensure MOngoDB is running via Docker Compose

## CLI commands

Ensure you have `go` installed and that the dependencies have been pulled in.

### Populate the database

```sh
go run ./cmd/cms update;
```

### Start the server

```sh
go run ./cmd/cms start;
```

## Getting stuff

For gRPC calls, ensure you have `grpcurl` installed and that the server has been started. Run any of the following below depending on what you're looking for. All endpoints accept a `limit` and `offset` parameter.

> The official docs for using `grpcurl` can be found [on this page](https://github.com/fullstorydev/grpcurl#invoking-rpcs)

### Getting authors

```sh
# http
curl -X GET "http://localhost:5476/authors?limit=10&offset=0";
# grpc
grpcurl -plaintext -d '{"limit":10,"offset":0}' localhost:5477 cms.ContentManagementSystem/GetAuthors;
```

### Getting categories

```sh
# http
curl -X GET "http://localhost:5476/categories?limit=10&offset=0";
# grpc
grpcurl -plaintext -d '{"limit":10,"offset":0}' localhost:5477 cms.ContentManagementSystem/GetCategories
```

### Getting feeds

```sh
# http
curl -X GET "http://localhost:5476/feeds?limit=10&offset=0";
# grpc
grpcurl -plaintext -d '{"limit":10,"offset":0}' localhost:5477 cms.ContentManagementSystem/GetFeeds;
```

### Getting items

```sh
# http
curl -X GET "http://localhost:5476/items?limit=10&offset=0";
# grpc
grpcurl -plaintext -d '{"limit":10,"offset":0}' localhost:5477 cms.ContentManagementSystem/GetItems;
```

[Back to main README.md](../README.md).
