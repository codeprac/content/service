# Deployment

This document contains information and instructions on how to deploy this service

- [Deployment](#deployment)
  - [Running on local machine](#running-on-local-machine)
  - [Helm charts](#helm-charts)
  - [Deploying to production](#deploying-to-production)
  - [Other notes](#other-notes)
    - [Convenience constructs](#convenience-constructs)
    - [Chart design](#chart-design)

## Running on local machine

To setup the deployment locally or to work on the deployment manifests you will need Kubernetes-in-Docker (`kind`). Start the development cluster by running the following in the [`./deploy`](../deploy) directory:

```sh
make init;

# to purge it later, run

make teardown;
```

Since `kind` uses it's own Docker registry, you will also need to build the image for `cms` locally and load that image into `kind` since the provided `sample` environment indicates to never pull the image. To do this:

1. Navigate to the project root and run `make image`
2. Navigate back to the `./deploy` directory and run `make load-image`

You can then proceed to deploy the Helm charts using the `make install` convenience recipe (instructions follow).

## Helm charts

Deployment is intended to be done via Helm charts to Kubernetes. The Helm charts can be found as directories in [`./deploy`](../deploy).

To deploy any of the charts, run the following from the `./deploy` directory:

```sh
helm upgrade --install \
  --values ./${NAME}/values/${ENV}.yaml \
  --values ./${NAME}/secrets/${ENV}.yaml \
  --values ./${NAME}/values.yaml \
  ${RELEASE_NAME} ./${NAME}
```

This is also packaged as a Makefile recipe:

```sh
make name=mongodb install;
```

## Deploying to production

To deploy to production, use `make env=production deploy` instead of `make install`. This circumvents the context check which ensures that the deployment is being done locally.

## Other notes

### Convenience constructs

For convenience of local deployments, a sample `sample.yaml` has been provided in both the `./deploy/values` and `./deploy/secrets` directory. **Do not use these in production, set the `env` to `"production"` when deploying to production**

### Chart design

While the `cms` deployment depends on `mongodb`, the charts are separate so that we can delete `cms` without worrying about accidentally removing the volumes from the `mongodb` deployment. This kinda improves resilience doncha think?

[Back to main README.md](../README.md).
